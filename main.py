import ipaddress
import random
import time
from datetime import datetime, timedelta
from log import logger
from prometheus_client import start_http_server, Counter, Summary, Gauge, Histogram

# 请求总数
request_total = Counter('demo_api_request_total', 'Total number of requests received')

# 请求速率 - 使用Counter与时间相关的统计请求速率
request_rate = Counter('demo_api_request_rate', 'Request rate per second')

# 请求延迟
request_latency = Histogram('demo_api_request_latency_seconds', 'Request latency in seconds')

# 错误率
error_rate = Counter('demo_api_error_rate', 'Error rate for requests')

# 请求大小
request_size = Histogram('demo_api_request_size_bytes', 'Size of the request')

# 响应大小
response_size = Histogram('demo_api_response_size_bytes', 'Size of the response')


def random_value(value):
    """
    传入生成随机值的列表，随机返回其中一个值
    """
    index = random.randint(0, len(value) - 1)
    return value[index]


def demo_log():
    """
    模拟生成debug日志，日志内容为access访问日志
    """
    content = {
        'access_status': random_value(
            [200] * 10 + [201] * 3 + [301, 400, 401, 403, 404, 500, 502, 503, 504]),
        'request_method': random_value(['GET'] * 10 + ['POST'] * 5 + ['PUT', 'DELETE']),
        'request_uri': random_value(['/login/'] * 5 + ['/account/'] * 4 + ['/public/'] * 3 + ['/management/']),
        'request_length': random.randrange(1, 99),
        'remote_address': str(ipaddress.ip_address(
            random.randint(int(ipaddress.ip_address('101.0.0.1')), int(ipaddress.ip_address('223.255.255.254'))))),
        'server_name': random_value(['cm-' + str(random.randrange(1, 9)) + '.cn',
                                     'cu-' + str(random.randrange(1, 9)) + '.cn']),
        'time_start': (datetime.now() - timedelta(milliseconds=random.randint(1, 1000))).strftime(
            "%Y-%m-%dT%H:%M:%S.%f")[:-3] + "+08:00",
        'time_finish': (datetime.now() + timedelta(milliseconds=random.randint(1, 1000))).strftime(
            "%Y-%m-%dT%H:%M:%S.%f")[:-3] + "+08:00",
        'http_user_agent': random_value(
            ['Firefox 7	Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0',
             'Chrome 9	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
             'Chrome 9	Mozilla/5.0 (Linux; Android 9; MX10 PRO) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36',
             'Mobile Safari	Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1',
             'Safari 11	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5',
             'Chrome 9	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
             'Chrome 9	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.58 Safari/537.36 Edg/93.0.961.33',
             'Firefox 7	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit Gecko/20100101 Firefox/78.0']),
    }
    if content['access_status'] in [500, 502, 503, 504]:
        logger.error(content)
    elif content['access_status'] in [400, 401, 403, 404]:
        logger.warning(content)
    else:
        # 随机模拟同一用户多次访问
        match random.randint(0, 9):
            case 0:
                logger.info(content)
                logger.info(content)
            case 2:
                logger.info(content)
                logger.info(content)
                logger.info(content)
            case 4:
                logger.info(content)
                logger.info(content)
                logger.info(content)
                logger.info(content)
            case 6:
                logger.info(content)
                logger.info(content)
                logger.info(content)
                logger.info(content)
                logger.info(content)
            case _:
                logger.info(content)


def process_request():
    """模拟处理请求并记录指标"""
    # 记录请求总数
    request_total.inc()

    # 记录请求速率
    request_rate.inc()

    # 模拟请求延迟
    latency = random.uniform(0.1, 2.0)
    request_latency.observe(latency)

    # 模拟请求大小
    req_size = random.uniform(100, 5000)
    request_size.observe(req_size)

    # 模拟响应大小
    resp_size = random.uniform(100, 5000)
    response_size.observe(resp_size)

    # 随机产生错误并记录错误率
    if random.random() < 0.1:  # 假设10%的请求出错
        error_rate.inc()

    # 模拟请求处理时间
    time.sleep(latency)


if __name__ == '__main__':
    # 启动Prometheus HTTP服务器
    start_http_server(9000)  # 监听端口9000
    while True:
        demo_log()
        process_request()
        time.sleep(random.random())
