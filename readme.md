# 项目简介

生成随机日志数据到指定文件中，模拟生产环境应用日志产生，以便对日志进行采集分析和存储操作。同时提供metric接口，以便进行监控指标采集。

# 运行
```bash
pip install -r requirements.txt
python main.py
```

# docker方式运行
```bash
[root@es-cold ~]# cd /opt/
[root@es-cold opt]# git clone https://gitee.com/cuiliang0302/log_demo.git
[root@es-cold opt]# cd log_demo/
[root@es-cold log_demo]# ls
Dockerfile  log.py  main.py  readme.md  requirements.txt
[root@es-cold log_demo]# docker build -t log_demo:1.0 .
[root@es-cold log_demo]# docker run -d -v $PWD/log:/opt/logDemo/log log_demo:1.0 
[root@es-cold log_demo]# cd logs 
[root@es-cold log]# ll
total 76
-rw-r--r-- 1 root root  5526 Jul 19 21:53 error.log
-rw-r--r-- 1 root root 28557 Jul 19 21:53 info.log
-rw-r--r-- 1 root root 12266 Jul 19 21:53 warning.log
```

# 日志样例
```bash
2023-07-20 22:50:12.475 | WARNING  | __main__:debug_log:47 - {'access_status': 401, 'request_method': 'PUT', 'request_uri': '/management/', 'request_length': 25, 'remote_address': '163.231.28.163', 'server_name': 'cu-25.cn', 'time_start': '2023-07-20T22:50:12.444+08:00', 'time_finish': '2023-07-20T22:50:13.270+08:00', 'http_user_agent': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Hot Lingo 2.0)'}
2023-07-20 22:50:13.023 | INFO     | __main__:debug_log:49 - {'access_status': 200, 'request_method': 'POST', 'request_uri': '/management/', 'request_length': 52, 'remote_address': '157.229.223.122', 'server_name': 'cu-23.cn', 'time_start': '2023-07-20T22:50:12.427+08:00', 'time_finish': '2023-07-20T22:50:13.152+08:00', 'http_user_agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36'}
2023-07-20 22:50:13.335 | ERROR    | __main__:debug_log:45 - {'access_status': 504, 'request_method': 'GET', 'request_uri': '/account/', 'request_length': 47, 'remote_address': '160.3.80.228', 'server_name': 'cm-2.cn', 'time_start': '2023-07-20T22:50:13.093+08:00', 'time_finish': '2023-07-20T22:50:13.488+08:00', 'http_user_agent': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Hot Lingo 2.0)'}
2023-07-20 22:50:14.158 | INFO     | __main__:debug_log:49 - {'access_status': 200, 'request_method': 'DELETE', 'request_uri': '/management/', 'request_length': 12, 'remote_address': '134.46.86.93', 'server_name': 'cm-4.cn', 'time_start': '2023-07-20T22:50:14.062+08:00', 'time_finish': '2023-07-20T22:50:14.663+08:00', 'http_user_agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3451.0 Safari/537.36'}
2023-07-20 22:50:14.380 | INFO     | __main__:debug_log:49 - {'access_status': 201, 'request_method': 'PUT', 'request_uri': '/account/', 'request_length': 29, 'remote_address': '203.30.85.45', 'server_name': 'cm-17.cn', 'time_start': '2023-07-20T22:50:14.244+08:00', 'time_finish': '2023-07-20T22:50:15.180+08:00', 'http_user_agent': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Hot Lingo 2.0)'}
2023-07-20 22:50:15.058 | INFO     | __main__:debug_log:49 - {'access_status': 200, 'request_method': 'GET', 'request_uri': '/account/', 'request_length': 58, 'remote_address': '172.38.27.228', 'server_name': 'cm-16.cn', 'time_start': '2023-07-20T22:50:14.900+08:00', 'time_finish': '2023-07-20T22:50:16.014+08:00', 'http_user_agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.2999.0 Safari/537.36'}
```
# 指标样例
```bash
# curl http://127.0.0.1:9000
# HELP python_gc_objects_collected_total Objects collected during gc
# TYPE python_gc_objects_collected_total counter
python_gc_objects_collected_total{generation="0"} 362.0
python_gc_objects_collected_total{generation="1"} 8.0
python_gc_objects_collected_total{generation="2"} 0.0
# HELP python_gc_objects_uncollectable_total Uncollectable objects found during GC
# TYPE python_gc_objects_uncollectable_total counter
python_gc_objects_uncollectable_total{generation="0"} 0.0
python_gc_objects_uncollectable_total{generation="1"} 0.0
python_gc_objects_uncollectable_total{generation="2"} 0.0
# HELP python_gc_collections_total Number of times this generation was collected
# TYPE python_gc_collections_total counter
python_gc_collections_total{generation="0"} 46.0
python_gc_collections_total{generation="1"} 4.0
python_gc_collections_total{generation="2"} 0.0
# HELP python_info Python platform information
# TYPE python_info gauge
python_info{implementation="CPython",major="3",minor="12",patchlevel="3",version="3.12.3"} 1.0
# HELP process_virtual_memory_bytes Virtual memory size in bytes.
# TYPE process_virtual_memory_bytes gauge
process_virtual_memory_bytes 1.8679808e+08
# HELP process_resident_memory_bytes Resident memory size in bytes.
# TYPE process_resident_memory_bytes gauge
process_resident_memory_bytes 2.7598848e+07
# HELP process_start_time_seconds Start time of the process since unix epoch in seconds.
# TYPE process_start_time_seconds gauge
process_start_time_seconds 1.73104334402e+09
# HELP process_cpu_seconds_total Total user and system CPU time spent in seconds.
# TYPE process_cpu_seconds_total counter
process_cpu_seconds_total 0.24
# HELP process_open_fds Number of open file descriptors.
# TYPE process_open_fds gauge
process_open_fds 10.0
# HELP process_max_fds Maximum number of open file descriptors.
# TYPE process_max_fds gauge
process_max_fds 1024.0
# HELP demo_api_request_total Total number of requests received
# TYPE demo_api_request_total counter
demo_api_request_total 46.0
# HELP demo_api_request_created Total number of requests received
# TYPE demo_api_request_created gauge
demo_api_request_created 1.7310433443704255e+09
# HELP demo_api_request_rate_total Request rate per second
# TYPE demo_api_request_rate_total counter
demo_api_request_rate_total 46.0
# HELP demo_api_request_rate_created Request rate per second
# TYPE demo_api_request_rate_created gauge
demo_api_request_rate_created 1.7310433443704562e+09
# HELP demo_api_request_latency_seconds Request latency in seconds
# TYPE demo_api_request_latency_seconds histogram
demo_api_request_latency_seconds_bucket{le="0.005"} 0.0
demo_api_request_latency_seconds_bucket{le="0.01"} 0.0
demo_api_request_latency_seconds_bucket{le="0.025"} 0.0
demo_api_request_latency_seconds_bucket{le="0.05"} 0.0
demo_api_request_latency_seconds_bucket{le="0.075"} 0.0
demo_api_request_latency_seconds_bucket{le="0.1"} 0.0
demo_api_request_latency_seconds_bucket{le="0.25"} 2.0
demo_api_request_latency_seconds_bucket{le="0.5"} 9.0
demo_api_request_latency_seconds_bucket{le="0.75"} 13.0
demo_api_request_latency_seconds_bucket{le="1.0"} 21.0
demo_api_request_latency_seconds_bucket{le="2.5"} 46.0
demo_api_request_latency_seconds_bucket{le="5.0"} 46.0
demo_api_request_latency_seconds_bucket{le="7.5"} 46.0
demo_api_request_latency_seconds_bucket{le="10.0"} 46.0
demo_api_request_latency_seconds_bucket{le="+Inf"} 46.0
demo_api_request_latency_seconds_count 46.0
demo_api_request_latency_seconds_sum 47.258851306489476
# HELP demo_api_request_latency_seconds_created Request latency in seconds
# TYPE demo_api_request_latency_seconds_created gauge
demo_api_request_latency_seconds_created 1.7310433443704784e+09
# HELP demo_api_error_rate_total Error rate for requests
# TYPE demo_api_error_rate_total counter
demo_api_error_rate_total 3.0
# HELP demo_api_error_rate_created Error rate for requests
# TYPE demo_api_error_rate_created gauge
demo_api_error_rate_created 1.7310433443705604e+09
# HELP demo_api_request_size_bytes Size of the request
# TYPE demo_api_request_size_bytes histogram
demo_api_request_size_bytes_bucket{le="0.005"} 0.0
demo_api_request_size_bytes_bucket{le="0.01"} 0.0
demo_api_request_size_bytes_bucket{le="0.025"} 0.0
demo_api_request_size_bytes_bucket{le="0.05"} 0.0
demo_api_request_size_bytes_bucket{le="0.075"} 0.0
demo_api_request_size_bytes_bucket{le="0.1"} 0.0
demo_api_request_size_bytes_bucket{le="0.25"} 0.0
demo_api_request_size_bytes_bucket{le="0.5"} 0.0
demo_api_request_size_bytes_bucket{le="0.75"} 0.0
demo_api_request_size_bytes_bucket{le="1.0"} 0.0
demo_api_request_size_bytes_bucket{le="2.5"} 0.0
demo_api_request_size_bytes_bucket{le="5.0"} 0.0
demo_api_request_size_bytes_bucket{le="7.5"} 0.0
demo_api_request_size_bytes_bucket{le="10.0"} 0.0
demo_api_request_size_bytes_bucket{le="+Inf"} 46.0
demo_api_request_size_bytes_count 46.0
demo_api_request_size_bytes_sum 120207.67160511808
# HELP demo_api_request_size_bytes_created Size of the request
# TYPE demo_api_request_size_bytes_created gauge
demo_api_request_size_bytes_created 1.7310433443705795e+09
# HELP demo_api_response_size_bytes Size of the response
# TYPE demo_api_response_size_bytes histogram
demo_api_response_size_bytes_bucket{le="0.005"} 0.0
demo_api_response_size_bytes_bucket{le="0.01"} 0.0
demo_api_response_size_bytes_bucket{le="0.025"} 0.0
demo_api_response_size_bytes_bucket{le="0.05"} 0.0
demo_api_response_size_bytes_bucket{le="0.075"} 0.0
demo_api_response_size_bytes_bucket{le="0.1"} 0.0
demo_api_response_size_bytes_bucket{le="0.25"} 0.0
demo_api_response_size_bytes_bucket{le="0.5"} 0.0
demo_api_response_size_bytes_bucket{le="0.75"} 0.0
demo_api_response_size_bytes_bucket{le="1.0"} 0.0
demo_api_response_size_bytes_bucket{le="2.5"} 0.0
demo_api_response_size_bytes_bucket{le="5.0"} 0.0
demo_api_response_size_bytes_bucket{le="7.5"} 0.0
demo_api_response_size_bytes_bucket{le="10.0"} 0.0
demo_api_response_size_bytes_bucket{le="+Inf"} 46.0
demo_api_response_size_bytes_count 46.0
demo_api_response_size_bytes_sum 116585.69996822249
# HELP demo_api_response_size_bytes_created Size of the response
# TYPE demo_api_response_size_bytes_created gauge
demo_api_response_size_bytes_created 1.7310433443706443e+09

```

# other文件
包含geoip插件数据库文件和grafana dashboard数据。