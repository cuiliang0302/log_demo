import os.path
from loguru import logger

LOG_DIR = "./logs"
info_log = os.path.join(LOG_DIR, "info.json")
warning_log = os.path.join(LOG_DIR, "warning.json")
error_log = os.path.join(LOG_DIR, "error.json")

# logger配置
config = {
    "rotation": "00:00",  # 每天0点生成新文件
    "encoding": "utf-8",
    "retention": "7 days",
    "backtrace": True,  # 异常回溯
    "serialize": True # 启用json序列化
}
logger.add(info_log, level='INFO', **config)
logger.add(warning_log, level='WARNING', **config)
logger.add(error_log, level='ERROR', **config)
