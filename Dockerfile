FROM python:3.12
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone && pip config set global.index-url https://mirrors.aliyun.com/pypi/simple/
ADD . /demo
WORKDIR /demo
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 5000
HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl -fs http://127.0.0.1:5000/health || exit 1
# 开发环境
CMD ["python3.11","-u","-m","flask","run","-h","0.0.0.0"]
# 生产环境
CMD ["gunicorn", "--bind", "0.0.0.0:5000" ,"app:app"]